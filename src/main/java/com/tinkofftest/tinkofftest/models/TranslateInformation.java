package com.tinkofftest.tinkofftest.models;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class TranslateInformation {
    private static final String DATE_FORMAT = "dd.MM.yyyy HH:mm:ss.SS";


    private Long id;

    private String text;

    private String from;

    private String to;

    private String ipAddress;

    private LocalDateTime timeCall;

    public TranslateInformation() {
    }

    public TranslateInformation(String text, String from, String to, String ipAddress) {
        this.text = text;
        this.from = from;
        this.to = to;
        this.ipAddress = ipAddress;
        timeCall = LocalDateTime.now();
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getLocalDateTime() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_FORMAT, Locale.ENGLISH);

        return timeCall.format(dtf);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

}
