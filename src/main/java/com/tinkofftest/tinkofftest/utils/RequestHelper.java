package com.tinkofftest.tinkofftest.utils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;

public class RequestHelper {

    private static final Logger LOGGER = Logger.getLogger(RequestHelper.class);

    private RequestHelper() {
    }

    public static JsonObject getRequestData(HttpServletRequest request) {
        StringBuilder buffer = new StringBuilder();
        String line;

        try (BufferedReader reader = request.getReader()) {
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }
        } catch (IOException e) {
            LOGGER.error(e);
        }

        return new JsonParser().parse(buffer.toString()).getAsJsonObject();
    }
}
