package com.tinkofftest.tinkofftest.utils;

import java.util.ResourceBundle;

public final class ResourceProvider {
    private static final ResourceBundle TEXT_EXPRESSION = ResourceBundle.getBundle("expressions/text_expressions");

    private static final ResourceBundle APPLICATION_PROPERTIES = ResourceBundle.getBundle("sql/db_connection");

    public static String getTextExpression(String key) {
        return TEXT_EXPRESSION.getString(key);
    }

    public static String getApplicationProperties(String key) {
        return APPLICATION_PROPERTIES.getString(key);
    }

    private ResourceProvider() {
    }

}
