package com.tinkofftest.tinkofftest.utils;

import com.google.gson.JsonObject;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

public final class ResponseHelper {
    private static final Logger LOGGER = Logger.getLogger(ResponseHelper.class);
    private static final String CONTENT_TYPE_JSON = "application/json";

    private ResponseHelper() {
    }

    public static void sendResponse(HttpServletResponse response, JsonObject jsonData) {
        response.setCharacterEncoding(StandardCharsets.UTF_8.name());
        try (PrintWriter out = response.getWriter()) {
            response.setContentType(CONTENT_TYPE_JSON);
            out.write(String.valueOf(jsonData));
            out.flush();
        } catch (IOException e) {
            LOGGER.error(e);
        }
    }
}
