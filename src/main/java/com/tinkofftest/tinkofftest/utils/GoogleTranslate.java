package com.tinkofftest.tinkofftest.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.tinkofftest.tinkofftest.models.TranslateInformation;
import org.apache.log4j.Logger;
import org.springframework.http.HttpMethod;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class GoogleTranslate {
    private static final Logger LOGGER = Logger.getLogger(GoogleTranslate.class);
    private static final String WHITE_SPACE = " ";
    private static final int THREADS_NUMBER = 5;
    private static final int TERMINATION_TIMEOUT = 10;
    private static final int CONNECT_TIMEOUT = 5000;
    private static final int READ_TIMEOUT = 5000;
    private TranslateInformation translateInformation;
    private String[] words;

    public GoogleTranslate(TranslateInformation translateInformation) {
        this.translateInformation = translateInformation;
        words = getWords(translateInformation.getText());

    }

    public String executeTranslate() {
        ExecutorService executor = Executors.newFixedThreadPool(THREADS_NUMBER);
        String[] result = new String[words.length];

        for (int i = 0; i < words.length; i++) {
            int position = i;
            executor.submit(() -> {
                String word = words[position];
                String query = getQuery(translateInformation.getFrom(), translateInformation.getTo(), word);
                HttpURLConnection connection = getGoogleTranslateConnection(query);
                result[position] = getResponseGoogle(connection);
            });

        }
        try {
            executor.shutdown();
            executor.awaitTermination(TERMINATION_TIMEOUT, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.error(e);
            Thread.currentThread().interrupt();
        }
        return getText(result);
    }

    private HttpURLConnection getGoogleTranslateConnection(String query) {
        HttpURLConnection con = null;
        try {
            URL url = new URL(query);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod(HttpMethod.GET.name());
            con.setConnectTimeout(CONNECT_TIMEOUT);
            con.setReadTimeout(READ_TIMEOUT);
        } catch (IOException e) {
            LOGGER.error(e);
        }
        return con;
    }

    private String getResponseGoogle(HttpURLConnection con) {
        String result = "";

        try (BufferedReader bf = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String input;
            StringBuilder content = new StringBuilder();

            while ((input = bf.readLine()) != null) {
                content.append(input);
            }

            result = getTranslatedTextFromResponse(content);
        } catch (IOException e) {
            LOGGER.error(e);
        }
        return result;
    }

    private String getTranslatedTextFromResponse (StringBuilder responseContent) {
        JsonArray jsonArray = (JsonArray) new JsonParser().parse(responseContent.toString());
        return  jsonArray.get(0).getAsJsonArray().get(0).getAsJsonArray().get(0).getAsString();
    }

    private String getText(String[] words) {
        StringBuilder stringBuilder = new StringBuilder();

        for (String element : words) {
            stringBuilder.append(element).append(WHITE_SPACE);
        }

        return stringBuilder.toString().trim();
    }

    private String deleteExtraSpaces(String text) {
        return text.trim().replaceAll("\\s+", WHITE_SPACE);
    }

    private String getQuery(String from, String to, String text) {
        StringBuilder sb = new StringBuilder();
        sb.append("https://translate.googleapis.com/translate_a/single?client=gtx&sl=");
        sb.append(from);
        sb.append("&tl=");
        sb.append(to);
        sb.append("&dt=t&q=");
        try {
            sb.append(URLEncoder.
                    encode(text, StandardCharsets.UTF_8.toString()));
        } catch (UnsupportedEncodingException e) {
            LOGGER.error(e);
        }
        return sb.toString();
    }

    private String[] getWords(String text) {
        return deleteExtraSpaces(text).split(WHITE_SPACE);
    }
}
