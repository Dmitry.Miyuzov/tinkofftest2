package com.tinkofftest.tinkofftest.controllers;

import com.google.gson.JsonObject;
import com.tinkofftest.tinkofftest.datalayer.DAO.DAOFactory;
import com.tinkofftest.tinkofftest.datalayer.DAO.DBType;
import com.tinkofftest.tinkofftest.datalayer.DAO.TranslateInformationDAO;
import com.tinkofftest.tinkofftest.models.TranslateInformation;
import com.tinkofftest.tinkofftest.utils.GoogleTranslate;
import com.tinkofftest.tinkofftest.utils.ResourceProvider;
import com.tinkofftest.tinkofftest.utils.RequestHelper;
import com.tinkofftest.tinkofftest.utils.ResponseHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
public class TranslateController {
    private static final String PATTERN_TEXT_EXPRESSIONS = ResourceProvider.getTextExpression("text.regexp");
    private static final String TEMPLATE_TRANSLATE = "translate";
    private static final String PARAMETER_TEXT = "text";
    private static final String PARAMETER_FROM = "from";
    private static final String PARAMETER_TO = "to";
    private static final Pattern TEXT_PATTERN = Pattern.compile(PATTERN_TEXT_EXPRESSIONS);
    private static final DAOFactory daoFactory = DAOFactory.getInstance(DBType.H2);


    @GetMapping("/translate")
    public String translate(Model model) {
        return TEMPLATE_TRANSLATE;
    }

    @PostMapping("/translate")
    public String executeTranslate(HttpServletRequest request, HttpServletResponse response, Model model) {
        TranslateInformation translateInformation = getDataFromRequest(request);
        TranslateInformationDAO translateInformationDAO = daoFactory.getTranslateInformationDAO();
        translateInformationDAO.save(translateInformation);
        if (isValidText(translateInformation.getText())) {
            String textTranslated = getTranslate(translateInformation);
            ResponseHelper.sendResponse(response, createResponse(textTranslated));
            response.setStatus(HttpServletResponse.SC_OK);
        }

        return TEMPLATE_TRANSLATE;
    }

    private boolean isValidText(String text) {
        boolean isValidate = false;
        Matcher matcher = TEXT_PATTERN.matcher(text);
        if (matcher.find()) {
            isValidate = true;
        }
        return isValidate;
    }

    private TranslateInformation getDataFromRequest(HttpServletRequest request) {
        JsonObject jsonObject = RequestHelper.getRequestData(request);
        String from = jsonObject.get(PARAMETER_FROM).getAsString();
        String to = jsonObject.get(PARAMETER_TO).getAsString();
        String text = jsonObject.get(PARAMETER_TEXT).getAsString();
        String ipAddress = request.getRemoteAddr();
        return new TranslateInformation(text, from, to, ipAddress);
    }

    private String getTranslate(TranslateInformation translateInformation) {
        GoogleTranslate googleTranslate = new GoogleTranslate(translateInformation);
        return googleTranslate.executeTranslate();
    }

    private JsonObject createResponse(String text) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(PARAMETER_TEXT, text);
        return jsonObject;
    }


}
