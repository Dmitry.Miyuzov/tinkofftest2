package com.tinkofftest.tinkofftest.datalayer.DAO;

import com.tinkofftest.tinkofftest.models.TranslateInformation;

public interface TranslateInformationDAO {
    void save(TranslateInformation translateInformation);
}
