package com.tinkofftest.tinkofftest.datalayer.DAO;


import com.tinkofftest.tinkofftest.datalayer.DB.H2TranslateInformationDAO;

public abstract class DAOFactory {

    public static DAOFactory getInstance(DBType dbType) {
        return dbType.getDAOFactory();
    }

    public abstract H2TranslateInformationDAO getTranslateInformationDAO();

}