package com.tinkofftest.tinkofftest.datalayer.DAO;

import com.tinkofftest.tinkofftest.datalayer.DB.H2DAOFactory;

public enum DBType {

    H2 {
        @Override
        public DAOFactory getDAOFactory() {
            return new H2DAOFactory();
        }
    };

    public abstract DAOFactory getDAOFactory();

}
