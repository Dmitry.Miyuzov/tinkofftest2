package com.tinkofftest.tinkofftest.datalayer.DB.connection;

import com.tinkofftest.tinkofftest.utils.ResourceProvider;
import org.h2.jdbcx.JdbcConnectionPool;

import java.sql.Connection;
import java.sql.SQLException;

public final class H2ConnectionPool {
    private static final String URL = "jdbc:h2:mem:tinkofftest;" + "INIT=RUNSCRIPT FROM 'src/main/resources/sql/schema.sql'\\;";
    private static final String USER_NAME = ResourceProvider.getApplicationProperties("db_name");
    private static final String PASS = ResourceProvider.getApplicationProperties("db_pass");
    private static JdbcConnectionPool jdbcConnectionPool = JdbcConnectionPool.create(URL, USER_NAME, PASS);

    private H2ConnectionPool() {
    }

    public static Connection getConnection() throws SQLException {


        return jdbcConnectionPool.getConnection();
    }
}
