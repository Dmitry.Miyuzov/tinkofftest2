package com.tinkofftest.tinkofftest.datalayer.DB;

import com.tinkofftest.tinkofftest.datalayer.DAO.TranslateInformationDAO;
import com.tinkofftest.tinkofftest.datalayer.DB.connection.H2ConnectionPool;
import com.tinkofftest.tinkofftest.models.TranslateInformation;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class H2TranslateInformationDAO implements TranslateInformationDAO {
    private static final Logger LOGGER = Logger.getLogger(H2TranslateInformationDAO.class);
    private static final String QUERY_SAVE = "save";
    private static final int PARAMETER_INDEX_ONE = 1;
    private static final int PARAMETER_INDEX_TWO = 2;
    private static final int PARAMETER_INDEX_THREE = 3;
    private static final int PARAMETER_INDEX_FOUR = 4;
    private static final int PARAMETER_INDEX_FIVE = 5;
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("sql/translate_information_query");


    @Override
    public void save(TranslateInformation translateInformation) {
        String query = resourceBundle.getString(QUERY_SAVE);
        try (Connection connection = H2ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(PARAMETER_INDEX_ONE, translateInformation.getText());
            statement.setString(PARAMETER_INDEX_TWO, translateInformation.getFrom());
            statement.setString(PARAMETER_INDEX_THREE, translateInformation.getTo());
            statement.setString(PARAMETER_INDEX_FOUR, translateInformation.getIpAddress());
            statement.setString(PARAMETER_INDEX_FIVE, translateInformation.getLocalDateTime());
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e);
        }
    }
}
