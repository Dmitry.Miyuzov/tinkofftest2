package com.tinkofftest.tinkofftest.datalayer.DB;

import com.tinkofftest.tinkofftest.datalayer.DAO.DAOFactory;

public class H2DAOFactory extends DAOFactory {

    @Override
    public H2TranslateInformationDAO getTranslateInformationDAO() {
        return new H2TranslateInformationDAO();
    }
}
