DROP TABLE IF EXISTS translate_information;
create table translate_information
(
    id            bigint auto_increment not null,
    text          varchar(255)          not null,
    from_language varchar(10)           not null,
    to_language   varchar(10)           not null,
    ip_address    varchar(20)           not null,
    request_date  timestamp             not null,

    primary key (id)
)